# Simple showcase for `knitr::spin`

These are mostly accompanying examples for the blogpost [3 R Markdown tricks to make report creation even easier](https://jozefhajnala.gitlab.io/r/r909-rmarkdown-tips/) 

- `src/spin_example.R` - an R script with custom formatted comments to be used as the source for creating reports with `knitr::spin()` and `rmarkdown::render()`
- `rendering_render.R` - an R script that uses `rmarkdown::render()` to create multiple different output reports based on `spin_example.R` and save them to `outputs/`
- `rendering_spin.R` - an R script that uses `knitr::spin()` to create multiple different output reports based on `spin_example.R` and save them to `outputs/`
- `outputs/` - HTML reports generated from the content of `spin_example.R` by running `rendering_spin.R` and `rendering_render.R`
- `css/` - Example css used for creating `outputs/ex_04_spin_air_css.html`, all credit for the `air.css` goes to [https://github.com/markdowncss/air](https://github.com/markdowncss/air)
