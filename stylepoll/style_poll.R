one <- function(a) {
  stopifnot(is.integer(a), length(a) == 1L)
  if (a == 1L) return("First")
  if (a == 2L) return("Second")
  "Other"
}

two <- function(a) {
  stopifnot(is.integer(a), length(a) == 1L)
  if (a == 1L)
    "First"
  else if (a == 2L)
    "Second"
  else
    "Other"
}

three <- function(a) {
  stopifnot(is.integer(a), length(a) == 1L)
  res <- "Other"
  if (a == 1L)
    res <- "First"
  else if (a == 2L)
    res <- "Second"
  res
}
