# My oldest R scripts (ca. 2007/08)

- Some of my oldest R codes I was able to find, unedited. 
- Dates somewhere to the end of 2007, beginning of 2008.
- This is here mostly as accompanying example for the blogpost [Why 2019 is a great year to start with R: A story of 10 year old R code](https://jozefhajnala.gitlab.io/r/r900-10-year-old-code/). And for the LOLs of course ;-)
- All the scripts should still be runnable