# ===================================================================
# Mnohorozmerna linearna regresia; pr. 13.3.1. v knihe Lamos, Potocky

y<-c(30,35,31,18,28,18,29)
z<-c(35,38,30,20,30,22,28)
x2<-c(15,21,18,9,14,9,12)
X<-cbind(rep(1,7),x2)
Y<-cbind(y,z)
S<-solve(t(X)%*%X)
Bh<-S%*%t(X)%*%Y
P<-diag(7)-X%*%S%*%t(X)
Sh<-(t(Y)%*%P%*%Y)/5
C1<-matrix(c(0,1),ncol=2)
P2<-X%*%S%*%t(C1)%*%solve(C1%*%S%*%t(C1))%*%C1%*%S%*%t(X)
Lam<-det(t(Y)%*%P%*%Y)/det(t(Y)%*%P%*%Y+t(Y)%*%P2%*%Y)
F<-2*(1-Lam)/Lam
qf(0.95,df1=2,df2=4)
1-pf(F,df1=2,df2=6)