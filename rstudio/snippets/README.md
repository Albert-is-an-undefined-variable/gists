# RStudio Code Snippets

Code snippets are text macros that are used for quickly inserting (and potentially executing) common chunks of code. To use them in your RStudio, copy-paste them to `Tools -> Global Options... -> Code -> Edit Snippets`.

To learn more, read:

* https://support.rstudio.com/hc/en-us/articles/204463668-Code-Snippets
* https://jozefhajnala.gitlab.io/r/r906-rstudio-snippets/

Currently present snippets:

- `attachpackages.r.snippets` - Attach useful dev packages quickly
- `debugmode.r.snippets` - Change the error option between recover and NULL for easier debugging
- `dputlike.r.snippets` - Place shareable definition of an object at cursor
- `function.r.snippets` - Insert customized function boilerplate
- `killAllTerminals.r.snippets` - Kill all RStudio terminals
- `livePreviewRmd.snippets` - Live preview for active R Markdown file on save
- `microbenchmark.r.snippets` - Boilerplate for microbenchmarking and boxplotting the results
- `serve_site.r.snippets` - Serves a blogdown site in the current working directory via a separate process in the terminal, waits until it is rendered and views the site in RStudio's Viewer
- `system.r.snippets` - On Shift+Tab, executes the text inserted after `$$` via system The result is then pasted at cursor. Use with care!
- `terminalExecute.r.snippets` - On Shift+Tab, executes the text inserted after `$$` in a new Terminal. Type in `$$top` and press Shift+Tab to execute top in a new Terminal. Use with care!
- `test_that.r.snippets` - Insert a unit testing chunk with context
- `tryCatch.r.snippets` - Insert acustomized tryCatch block